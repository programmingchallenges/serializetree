import unittest
import main
from Node import Node

class TestStringMethods(unittest.TestCase):

    def testBasic(self):
        node = Node('root', Node('left', Node('left.left')), Node('right'))
        self.assertEqual(main.deserialize(main.serialize(node)).left.left.val, 'left.left')

    def testRootOnly(self):
        node = Node('root')
        self.assertEqual(main.deserialize(main.serialize(node)).val, 'root')
        self.assertEqual(main.deserialize(main.serialize(node)).right, None)

    def testLeftTree(self):
        node = Node('root', Node('left', Node('left.left', Node('left.left.left'))))
        self.assertEqual(main.deserialize(main.serialize(node)).left.left.left.val, 'left.left.left')

    def testRightTree(self):
        node = Node('root', None, Node('right', None, Node('right.right', None, Node('right.right.right'))))
        self.assertEqual(main.deserialize(main.serialize(node)).right.right.right.val, 'right.right.right')

if __name__ == '__main__':
    unittest.main()