import sys
from Node import Node
  

def serialize(rootNodeOfTree):
    return serializeHelper(rootNodeOfTree)    

# def serializeHelper(node):
#     if node == None:
#         return ''
#     elif node.left != None and node.right != None:
#         return node.val + '\n' + serializeHelper(node.left) + ' ' + serializeHelper(node.right) + '\n'
#     elif node.right != None:
#         return node.val + '\nNone ' + serializeHelper(node.right) + '\n'
#     elif node.left != None:
#         return node.val + '\n' + serializeHelper(node.left) + ' None\n'
#     else:
#         return node.val + '\nNone None\n'

def serializeHelper(node):
    if node == None:
        return ''
    elif node.left != None and node.right != None:
        return node.val + ' ' + node.left.val + ' ' + node.right.val + '\n' + serializeHelper(node.left) + serializeHelper(node.right)
    elif node.right != None:
        return node.val + ' None ' + node.right.val + '\n' + serializeHelper(node.right)
    elif node.left != None:
        return node.val + ' ' + node.left.val + ' None\n' + serializeHelper(node.left)
    else:
        return node.val + ' None None\n'


def deserialize(stringReprsentationOfTree):
    nodeMap = {}
    rootVal = ''
    for line in stringReprsentationOfTree.split('\n'):
        valLeftRight = line.split(' ')
        if rootVal == '':
            rootVal = valLeftRight[0]
        if len(valLeftRight) > 1:
            nodeMap[valLeftRight[0]] = [valLeftRight[1], valLeftRight[2]]

    return deserializeNodes(rootVal, nodeMap)

def deserializeNodes(curNodeVal, nodeMap):
    curMapEntry = nodeMap[curNodeVal]
    if curMapEntry[0] == 'None' and curMapEntry[1] == 'None':
        return Node(curNodeVal)
    elif curMapEntry[0] == 'None':
        return Node(curNodeVal, None, deserializeNodes(curMapEntry[1], nodeMap))
    elif curMapEntry[1] == 'None':
        return Node(curNodeVal, deserializeNodes(curMapEntry[0], nodeMap))
    else:
        return Node(curNodeVal, deserializeNodes(curMapEntry[0], nodeMap), deserializeNodes(curMapEntry[1], nodeMap))

if __name__ == '__main__':
    print('No main for this file')
